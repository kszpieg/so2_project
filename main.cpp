#include <iostream>
#include <ncurses.h>
#include "Client.h"
#include <chrono>
#include <thread>
#include <time.h>
#include <string>
#include "Service.h"
#include <mutex>

int numberOfBalls;
int numberOfService = 4;
int n = 0;
int ns = 0;
bool isStrike = false;
char **products;
int line = 0;
int **que;

char base[24][22] = { 
							{'/', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '\\' },
							{'|', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '|' },
							{'|', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '|' },
							{'|', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '|' },
							{'|', 'P', ' ', ' ', ' ', 'W', 'F', ' ', ' ', ' ', 'M', 'N', ' ', ' ', ' ', 'H', 'D', ' ', ' ', ' ', 'A', '|' },
							{'|', 'P', ' ', ' ', ' ', 'W', 'F', ' ', ' ', ' ', 'M', 'N', ' ', ' ', ' ', 'H', 'D', ' ', ' ', ' ', 'A', '|' },
							{'|', 'P', ' ', ' ', ' ', 'W', 'F', ' ', ' ', ' ', 'M', 'N', ' ', ' ', ' ', 'H', 'D', ' ', ' ', ' ', 'A', '|' },
							{'|', 'P', ' ', ' ', ' ', 'W', 'F', ' ', ' ', ' ', 'M', 'N', ' ', ' ', ' ', 'H', 'D', ' ', ' ', ' ', 'A', '|' },
							{'|', 'P', ' ', ' ', ' ', 'W', 'F', ' ', ' ', ' ', 'M', 'N', ' ', ' ', ' ', 'H', 'D', ' ', ' ', ' ', 'A', '|' },
							{'|', 'P', ' ', ' ', ' ', 'W', 'F', ' ', ' ', ' ', 'M', 'N', ' ', ' ', ' ', 'H', 'D', ' ', ' ', ' ', 'A', '|' },
							{'|', 'P', ' ', ' ', ' ', 'W', 'F', ' ', ' ', ' ', 'M', 'N', ' ', ' ', ' ', 'H', 'D', ' ', ' ', ' ', 'A', '|' },
							{'|', 'P', ' ', ' ', ' ', 'W', 'F', ' ', ' ', ' ', 'M', 'N', ' ', ' ', ' ', 'H', 'D', ' ', ' ', ' ', 'A', '|' },
							{'|', 'P', ' ', ' ', ' ', 'W', 'F', ' ', ' ', ' ', 'M', 'N', ' ', ' ', ' ', 'H', 'D', ' ', ' ', ' ', 'A', '|' },
							{'|', 'P', ' ', ' ', ' ', 'W', 'F', ' ', ' ', ' ', 'M', 'N', ' ', ' ', ' ', 'H', 'D', ' ', ' ', ' ', 'A', '|' },
							{'|', 'P', ' ', ' ', ' ', 'W', 'F', ' ', ' ', ' ', 'M', 'N', ' ', ' ', ' ', 'H', 'D', ' ', ' ', ' ', 'A', '|' },
							{'|', 'P', ' ', ' ', ' ', 'W', 'F', ' ', ' ', ' ', 'M', 'N', ' ', ' ', ' ', 'H', 'D', ' ', ' ', ' ', 'A', '|' },
							{'|', 'P', ' ', ' ', ' ', 'W', 'F', ' ', ' ', ' ', 'M', 'N', ' ', ' ', ' ', 'H', 'D', ' ', ' ', ' ', 'A', '|' },
							{'|', 'P', ' ', ' ', ' ', 'W', 'F', ' ', ' ', ' ', 'M', 'N', ' ', ' ', ' ', 'H', 'D', ' ', ' ', ' ', 'A', '|' },
							{'|', 'P', ' ', ' ', ' ', 'W', 'F', ' ', ' ', ' ', 'M', 'N', ' ', ' ', ' ', 'H', 'D', ' ', ' ', ' ', 'A', '|' },
							{'|', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '|' },
							{'|', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '|' },
							{'|', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '|' },
							{'|', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '|' },
							{'\\', '-', '_', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', 'Y', '/' }
						};

Client *balls[100];
Service *ss[5];
std::thread th[100];
std::thread ths[5];
std::mutex mtx;

void runBall(Client *b)
{
	b->move();
}

void runService(Service *s)
{
	s->move();
}

void draw()
{
	int stop;

	while (true)
	{
		stop = 0;
		clear();

		for (int y = 0; y < 24; y++)
		{
			for (int x = 0; x < 22; x++)
			{
				printw("%c", products[y][x]);
			}
			printw("\n");
		}
		
		std::unique_lock<std::mutex> lock(mtx);

		for (int i = 0; i < n; i++)
		{
			move(balls[i]->getY(), balls[i]->getX());
			if (balls[i]->getLife())
			{
				printw("x");
			}
			else
			{
				printw("");
				char *bp = balls[i]->getBp();
				int bpCap = balls[i]->getBpCap();
				move(i, 25);
				printw("Klient[%d] kupil: ", i);
				for (int x = 0; x < bpCap; x++)
				{
					printw("%c", bp[x]);
				}
				stop++;
			}
		}


		for (int i = 0; i < ns; i++)
		{
			move(ss[i]->getY(), ss[i]->getX());
			if (ss[i]->getLife())
			{
				printw("o");
			}
			else
			{
				printw("");
			}
		}
		if(!isStrike)
			isStrike = (rand()%400 == 357) ? true : false;
		lock.unlock();

		refresh();

		std::this_thread::sleep_for(std::chrono::milliseconds(50));

		if (stop == numberOfBalls)
			break;
	}
}

int main(int argc, char const *argv[])
{
	if (argc <= 1)
	{
		numberOfBalls = 3;
	}
	else
	{		
		std::string tmp = argv[1];
		numberOfBalls = atoi(tmp.c_str());
	}

	products = new char *[24];
	que = new int *[24];

	for (int y = 0; y < 24; y++)
	{
		products[y] = new char [22];
		que[y] = new int [22];
	}

	for (int y = 0; y < 24; y++)
	{
		for(int x = 0; x < 22; x++)
		{
			if (x == 21 && y > 0 && y < 23)
				products[y][x] = '|';
			else
				products[y][x] = base[y][x];

			que[y][x] = 0;
		}
	}

	srand(time(NULL));
	initscr();

	std::thread d(draw);

	for (int i = 0; i < numberOfBalls; i++)
	{
		balls[i] = new Client(&mtx, products, que);
		th[i] = std::thread(runBall, balls[i]);
		n++;
		std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	}

	for (int i = 0; i < numberOfService; i++)
	{
		ss[i] = new Service(&mtx, products, &isStrike);
		ths[i] = std::thread(runService, ss[i]);
		ns++;
		std::this_thread::sleep_for(std::chrono::milliseconds(9000));
	}

	for (int i = 0; i < numberOfBalls; i++)
	{
		th[i].join();
	}

	for (int i = 0; i < numberOfService; i++)
	{
		ths[i].join();
	}

	d.join();

	getch();
	endwin();
	
	return 0;
}