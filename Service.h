#include <ncurses.h>
#include <iostream>
#include <stdlib.h>
#include <chrono>
#include <thread>
#include <mutex>

class Service
{
private:
	bool life, ready, *isStrike, strike, endStrike;
	int x, y, t, moveX, moveY, lastX, lastY;
	std::mutex* mtx;
	char **pro;
public:
	Service(std::mutex* mtx, char **products, bool *isStrike);
	~Service();
	void move();
	int getX();
	int getY();
	int getMoveX();
	int getMoveY();
	int getLife();
};