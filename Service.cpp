#include "Service.h"

Service::Service(std::mutex* mtx, char **products, bool *isStrike)
{
	y = 2;
	x = 1;
	life = true;
	ready = false;
	strike = false;
	endStrike = true;
	this->isStrike = isStrike;
	t = 100;
	moveX = 1;
	moveY = 0;
	pro = products;
	this->mtx = mtx;
}

Service::~Service() {}

void Service::move()
{
	std::this_thread::sleep_for(std::chrono::milliseconds(t));

	while(life)
	{
		if (!strike)
		{
			std::this_thread::sleep_for(std::chrono::milliseconds(t));
			
			if(!ready)
			{
				std::unique_lock<std::mutex> lock(*mtx);
				x += moveX;
				y += moveY;

				if (x == 2 && pro[y][x-1] == '.')
				{
					pro[y][x-1] = 'P';
				}
				else if (x == 7 && pro[y][x-1] == '.')
				{
					pro[y][x-1] = 'F';
				}
				else if (x == 12 && pro[y][x-1] == '.')
				{
					pro[y][x-1] = 'N';
				}
				else if (x == 17 && pro[y][x-1] == '.')
				{
					pro[y][x-1] = 'D';
				}
				else if (x == 4 && pro[y][x+1] == '.')
				{
					pro[y][x+1] = 'W';
				}
				else if (x == 9 && pro[y][x+1] == '.')
				{
					pro[y][x+1] = 'M';
				}
				else if (x == 14 && pro[y][x+1] == '.')
				{
					pro[y][x+1] = 'H';
				}
				else if (x == 19 && pro[y][x+1] == '.')
				{
					pro[y][x+1] = 'A';
				}

				if((*isStrike))
					strike = (*isStrike);
//				strike = (rand()%200 == 50) ? true : false;

				if(strike)
				{
					lastX = x;
					lastY = y;
					endStrike = false;
				}

				if (y == 3)
				{
					moveX = 1;
					moveY = 0;
				}

				if (x%5 == 2)
				{
					moveY = 1;
					moveX = 0;
				}

				if (y == 19)
				{
					moveX = 1;
					moveY = 0;
				}

				if (x%5 == 4 && y != 3)
				{
					moveY = -1;
					moveX = 0;
				}

				if (x == 19 && y == 3)
					ready = true;
				
				lock.unlock();
			}
			else
			{
				if (y != 2)
					y--;
				else if (y == 2 && x != 1)
					x--;
				else if (y == 2 && x == 1)
				{
					ready = false;
					std::this_thread::sleep_for(std::chrono::milliseconds(3000));
				}
			}
		}
		else
		{
			std::this_thread::sleep_for(std::chrono::milliseconds(50));

			if(!endStrike)
			{
				if (y != 22 && x%5 <= 1)
					x++;
				else if (y != 22 && x%5 > 1)
					y++;
				else if (y == 22 && x != 1)
					x--;
				else if (y == 22 && x == 1)
				{
					std::this_thread::sleep_for(std::chrono::milliseconds(5000));
					endStrike = true;
				}
			}
			else
			{
				if (y == 22 && x == 1)
					x++;
				else if (lastX%5 > 1 && x != lastX)
					x++;
				else if (lastX%5 <= 1 && y != lastY)
					y--;
				else if (x == lastX && y != lastY)
					y--;
				else if (y == lastY && x != lastX)
					x++;
				else if (x == lastX && y == lastY)
				{
					strike = false;
					(*isStrike) = strike;
				}
			}
		}
	}
}

int Service::getX()
{
	return x;
}

int Service::getY()
{
	return y;
}

int Service::getLife()
{
	return life;
}

int Service::getMoveX()
{
	return moveX;
}

int Service::getMoveY()
{
	return moveY;
}