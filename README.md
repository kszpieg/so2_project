**Ireneusz Krawczyk 226214**
**Krzysztof Szpieg 226162**

#Systemy operacyjne 2
##Projekt 2


Program będzie realizował symulację zakupów w sklepie sieci Stonka. Zasada działania będzie wyglądać następująco: aplikacja zostanie oparta na wątkach, którymi staną się klienci chodzący po poszczególnych alejkach z produktami. Sklep będzie miał z góry ustaloną wielkość, ale towary na półkach i ich ceny będą losowane. Klienci będą chodzić po sklepie i z określonym prawdopodobieństwem będą ładować dane produkty do koszyka. Zmienna klienta przechowywałaby sumę do zapłaty przy kasie, która zostałaby wyświetlana na ekranie nabijając licznik obsłużonego klienta gdzie jego wątek by się kończył. Całość zostanie zaprezentowana na ekranie z wykorzystaniem biblioteki ncurses.

##TO DO (w najbliższym)
1. Strajki
2. Znikające produkty
3. Naliczanie kasy za zakupy
4. Synchronizacja wątków