#include "Client.h"

Client::Client(std::mutex* mtx, char **products, int **que)
{
	y = 22;
	x = 2;
	life = true;
	ready = false;
	t = 200;
	bpCap = 0;
	bpCapMax = rand()%10+1;
	moveX = (rand()%2 == 0) ? 1 : -1;
	moveY = -1;
	bp = new char[bpCapMax];
	pro = products;
	quee = que;
	this->mtx = mtx;
}

Client::~Client() {
	delete bp;
}

void Client::move()
{
	std::this_thread::sleep_for(std::chrono::milliseconds(t));

	while(life)
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(t));

		if(!ready)
		{
			std::unique_lock<std::mutex> lock(*mtx);
			x += moveX;
			y += moveY;
		
			if ( y >= 4 && y <= 18)
			{
				if( x == 2 )
				{
					if( rand()%5 == 3 && pro[y][x-1] != '.')
					{
						bp[bpCap++] = 'P';
						pro[y][x-1] = '.';
					}
				}

				if( x == 4 )
				{
					if( rand()%5 == 3 && pro[y][x+1] != '.')
					{
						bp[bpCap++] = 'W';
						pro[y][x+1] = '.';
					}
				}

				if( x == 7 )
				{
					if( rand()%5 == 3 && pro[y][x-1] != '.')
					{
						bp[bpCap++] = 'F';
						pro[y][x-1] = '.';
					}	
				}

				if( x == 9 )
				{
					if( rand()%5 == 3 && pro[y][x+1] != '.')
					{
						bp[bpCap++] = 'M';
						pro[y][x+1] = '.';
					}
				}

				if( x == 12 )
				{
					if( rand()%5 == 3 && pro[y][x-1] != '.')
					{
						bp[bpCap++] = 'N';
						pro[y][x-1] = '.';
					}
				}

				if( x == 14 )
				{
					if( rand()%5 == 3 && pro[y][x+1] != '.')
					{
						bp[bpCap++] = 'H';
						pro[y][x+1] = '.';
					}
				}

				if( x == 17 )
				{
					if( rand()%5 == 3 && pro[y][x-1] != '.')
					{
						bp[bpCap++] = 'D';
						pro[y][x-1] = '.';
					}
				}

				if( x == 19 )
				{
					if( rand()%4 == 3 && pro[y][x+1] != '.')
					{
						bp[bpCap++] = 'A';
						pro[y][x+1] = '.';
					}
				}
			}

			if (bpCap == bpCapMax)
				ready = true;

			if ( x%5 == 2 || x%5 == 4)
			{
				if ( y == 3 || y == 19)
				{
					x += moveX;
				}
			}

			if ( ( (y <= 4 || y >= 18) && (x == 1 || x == 20) ) || ( (y > 3 && y < 19) && (x%5 == 2 || x%5 == 4) ) )
			{
				moveX = -moveX;
				
			}

			if ( (y == 1 || y == 22) && (x%5 != 1 && x%5 != 0) || ( (y == 1 || y == 3 || y == 19 || y == 22) && (x%5 == 1 || x%5 == 0) ) )
			{
				moveY = -moveY;
			}

			lock.unlock();
		}
		else
		{
			if (y != 22)
			{
				if(quee[y+1][x] != 1)
				{
					quee[y][x] = 0;
					y++;
				}
				else
				{
					quee[y][x] = 1;
				}
			}
			else if ( y == 22 && x != 20)
			{
				if(quee[y][x+1] != 1)
				{
					quee[y][x] = 0;
					x++;	
				}
				else
				{
					quee[y][x] = 1;
				}
			}
			else if ( y == 22 && x == 20)
			{
				quee[y][x] = 1;
				std::this_thread::sleep_for(std::chrono::milliseconds(4000));
				life = false;
				quee[y][x] = 0;
			}
		}
	}
}

int Client::getX()
{
	return x;
}

int Client::getY()
{
	return y;
}

int Client::getLife()
{
	return life;
}

int Client::getMoveX()
{
	return moveX;
}

int Client::getMoveY()
{
	return moveY;
}

char *Client::getBp()
{
	return bp;
}

int Client::getBpCap()
{
	return bpCap;
}