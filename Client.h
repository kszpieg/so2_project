#include <ncurses.h>
#include <iostream>
#include <stdlib.h>
#include <chrono>
#include <thread>
#include <mutex>

class Client
{
private:
	bool life, ready;
	int x, y, t, moveX, moveY, bpCap, bpCapMax;
	char *bp; //koszyk
	char **pro;
	int **quee;
	std::mutex* mtx;
public:
	Client(std::mutex* mtx, char **products, int **que);
	~Client();
	void move();
	int getX();
	int getY();
	int getMoveX();
	int getMoveY();
	int getLife();
	char *getBp();
	int getBpCap();
};